<?php 
/**
 *  Start loading up theme helper features
 */
require_once __DIR__ . '/includes/helpers.php';


/**
 *  Plugins
 */
// Exact Target Integration
// require_once __DIR__ . '/plugins/cima-exact-target/cima-exact-target.php';
require_once dirname(__FILE__). '/newsletter.php';

// Handles post-form submission newsetter signup.
require_once dirname(__FILE__). '/cf7-newsletter.php';

// CIMA Timber extensions
require_once dirname(__FILE__). '/cima-timber.php';

// CIMA Job Board 
require_once dirname(__FILE__). '/cima-job-board.php';

// CIMA Member Directory
require_once dirname(__FILE__). '/cima-member-directory.php';

// Form Handler for  Volunteer Speak and Sponsor Forms
require_once dirname(__FILE__). '/form-handler.php';

// Fetches and caches FB Albums  for CIMA
require_once dirname(__FILE__). '/cima-fb-gallery.php';

// Member Verification Logic 
require_once dirname(__FILE__). '/cima-verify.php';

/**
 * Load 'Features'
 */
add_action( 'after_setup_theme', 'fcc_load_features' );
function fcc_load_features(){
  $path = __DIR__ . '/features/';
  
  $features = array(
    'add-admin-ajax-path',
    'admin-menu-customization',
    'copyright-range',
    'current_page_cpt-archives',
    'custom-dashicons',
    'include-typekit',
    'jquery-from-cdn',
    'login-redirect',
    'slug-in-body-class',
    'wp-head-cleanup'
  );

  foreach ( $features as $feature ) {
    require $path.$feature.'.php';
  }
}

/**
 * Load Custom Post Types
 */
add_action( 'after_setup_theme', 'fcc_load_post_types' );
function fcc_load_post_types(){
  $path = __DIR__ . '/post-types/';
  
  $post_types = array(
   
    'event-speakers',
    'event-sponsors',
    'great-efforts',
    'board-members',
    'job-postings',
    'knowledge-center'

  );

  foreach ( $post_types as $post_type ) {
    require $path.$post_type.'.php';
  }
}

