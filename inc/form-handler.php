<?php 

require_once 'et_client.php';

class FormHandler {

    public $et_response;

    function __construct(){
        add_action('init', array($this,'ajax_setup'));
    }

    public function ajax_setup(){
        add_action('wp_ajax_submit_form', array($this, 'form_handler'));
        add_action('wp_ajax_nopriv_submit_form', array($this, 'form_handler'));
    }

    public function subscribe_to_newsletter($email){

        $this->et_response = ET_Client::subscribe($email);

    }

    public function get_to_email($form_name){
        switch ($form_name) {
            case 'volunteer':
                return "volunteer@chicagoima.org";
                break;

            case 'sponsor':
                return "sponsorship@chicagoima.org";
                break;

            case 'speak':
                return "speakers@chicagoima.org";
                break;

            case 'board':
                return 'info@chicagoima.org';
                break;

            case 'contact':
                return 'info@chicagoima.org';
                break;

            default:
                return 'info@chicagoima.org';
                break;
        }
    }

    public function form_handler(){
        if (!empty($_POST['data'])){

            $form_name = isset($_POST['data']['form_name']) ? $_POST['data']['form_name'] : null;

            $first_name = isset($_POST['data']['first_name']) ? $_POST['data']['first_name'] : null;
            $last_name = isset($_POST['data']['last_name']) ? $_POST['data']['last_name'] : null;
            $full_name =  "$first_name $last_name"; 

            $company = isset($_POST['data']['company_name']) ? $_POST['data']['company_name'] : null;
            $title = isset($_POST['data']['title']) ? $_POST['data']['title'] : null;
            $industry = isset($_POST['data']['industry']) ? $_POST['data']['industry'] : null;

            $email = isset($_POST['data']['email']) ? sanitize_email( $_POST['data']['email'] ) : null;
            $phone = isset($_POST['data']['phone']) ?  $_POST['data']['phone']  : null;
            
            $how_can_help = isset($_POST['data']['how_can_help']) ?  $_POST['data']['how_can_help']  : null;

            $is_cima_member = isset($_POST['data']['is_cima_member']) ?  $_POST['data']['is_cima_member']  : null;
            $newsletter = isset($_POST['data']['newsletter']) ?  $_POST['data']['newsletter']  : null;

            // Sign them up for newsletter, if they want it..
            if ((true == (bool) $newsletter) && $email) {
                $signed_up = "true";
                $this->subscribe_to_newsletter($email, $first_name, $last_name);
            }

            ob_start();

            echo "New Message from the " . $form_name . " form:<br/><br/>";

            if ($full_name) echo "Name:     $full_name <br/>";
            if ($company)   echo "Company:  $company <br/>";
            if ($title)     echo "Title:    $title <br/>";
            if ($industry)  echo "Industry: $industry <br/>";
            if ($email)     echo "Email:    $email <br/>";
            if ($phone)     echo "Phone:    $phone <br/><br/>";

            if ($how_can_help) 
                echo  "How I can help: $how_can_help <br/></br>";

            if ($is_cima_member) echo "Is A CIMA Member?: $is_cima_member <br/>";
            if ($newsletter) echo "Signed up for newsletter: $newsletter  <br/>";
            
            $message = ob_get_contents();
            ob_end_clean();

           

            $to = $this->get_to_email($form_name);
            $subject = "New message from " . $form_name ." form";
            $headers = 'From: ChicagoIMA.org <no-reply@chicagoima.org>' . "\r\n";

            $response = wp_mail( $to, $subject, $message, $headers);

            $resp = $response == 1 ? "success" : "fail";
            die($response);
        } 


    }

}

global $form_handler;
$form_handler = new FormHandler;