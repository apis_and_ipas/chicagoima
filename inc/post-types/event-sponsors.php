<?php 

add_action( 'init', 'sponsor_post_type'  );

function sponsor_post_type(){
    register_post_type( 'sponsor', array(
      'public' => true,
      // 'map_meta_cap' => true,
      'capability_type' => 'sponsor',
      'rewrite' => false,
      'supports' => array( 'title'),
      'show_ui' => true,
      'show_in_menu' => 'edit.php?post_type=event',
      'has_archive' => false,
      'labels' => array(
          'name' => 'Sponsors',
          'singular_name' => 'Sponsor',
          'add_new_item' => 'New Sponsor',
          'edit_item' => 'Edit Sponsor'
      )
    ));
}