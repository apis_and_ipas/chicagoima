<?php 

add_action( 'init', 'speaker_post_type'  );

function speaker_post_type(){
    register_post_type( 'speaker', array(
      'public' => true,
      // 'map_meta_cap' => true,
      'capability_type' => 'speaker',
      'rewrite' => false,
      'supports' => array( 'title', 'editor', 'thumbnail'),
      'show_ui' => true,
      'show_in_menu' => 'edit.php?post_type=event',
      'has_archive' => false,
      'labels' => array(
          'name' => 'Speakers',
          'singular_name' => 'Speaker',
          'add_new_item' => 'New Speaker',
          'edit_item' => 'Edit Speaker'
      )
    ));
}