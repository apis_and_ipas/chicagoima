<?php 

add_action( 'init', 'great_effort_post_type'  );

function great_effort_post_type(){
    register_post_type( 'great-effort', array(
      'public' => true,
      'map_meta_cap' => true,
      'rewrite' => false,
      'capability_type' => 'great-effort',
      'supports' => array( 'title', 'editor', 'thumbnail' ),
      'show_ui' => true,
      'menu_icon' => '',
      'has_archive' => false,
      'labels' => array(
          'name' => 'Great Efforts',
          'singular_name' => 'Great Effort',
          'add_new_item' => 'New Great Effort',
          'edit_item' => 'Edit Great Effort'
      )
    ));
}