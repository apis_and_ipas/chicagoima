<?php 

add_action( 'init', 'job_posting_post_type'  );

function job_posting_post_type(){
    register_post_type( 'job_posting', array(
      'public' => true,
      'map_meta_cap' => true,
      'capability_type' => 'job_posting',
      'rewrite' => array('slug' => 'jobs'),
      'supports' => array( 'title', 'author'),
      'show_ui' => true,
      'menu_icon' => '',
      'has_archive' => true,
      'labels' => array(
          'name' => 'Job Postings',
          'singular_name' => 'Job Post',
          'add_new_item' => 'New Job Post',
          'edit_item' => 'Edit Job Post'
      )
    ));
}



