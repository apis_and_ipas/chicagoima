<?php 

add_action( 'init', 'kc_post_type'  );
function kc_post_type(){
    register_post_type( 'kc-post', array(
      'public' => true,
      'map_meta_cap' => true,
      'capability_type' => 'kc-post',
      'rewrite' => array('slug' => 'knowledge-center'),
      'supports' => array( 'title', 'editor' ),
      'show_ui' => true,
      'menu_icon' => '',
      'has_archive' => true,
      'labels' => array(
          'name' => 'Knowledge Ctr',
          'singular_name' => 'Knowledge Center Post',
          'add_new_item' => 'New Knowledge Center Post',
          'edit_item' => 'Edit Knowledge Center Post'
      )
    ));

    register_taxonomy( 'type', 'kc-post', array(
        'label' => 'Type',
        'hierarchical' => false,
        'show_ui' => true,
        'required' => true,
        'single_value' => true
    ));

}