<?php 

add_action( 'init', 'board_members_post_type'  );

function board_members_post_type(){
    register_post_type( 'board-member', array(
      'public' => true,
      // 'map_meta_cap' => true,
      'rewrite' => false,
      'capability_type' => 'board-member',
      'supports' => array( 'title', 'thumbnail', 'editor'),
      'show_ui' => true,
      'menu_icon' => '',
      // 'show_in_menu' => true,
      'has_archive' => false,
      'labels' => array(
          'name' => 'Board Members',
          'singular_name' => 'Board Member',
          'add_new_item' => 'New Board Member',
          'edit_item' => 'Edit Board Member'
      )
    ));
}