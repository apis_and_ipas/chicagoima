<?php 

/**
 *  Extends the Timber Class to allow full control of context overrides / additions
 *
 */
if (class_exists('Timber')) {
    class CIMA extends Timber{

        /**
        *  Overrides Timber::get_context with helpful global values.
        */
        public static function get_context(){
            $context = parent::get_context();
            
            $context['asset_path'] = get_stylesheet_directory_uri();

            $context['header'] = array();
            $context['header']['menu'] = new TimberMenu('header_menu');
            $context['header']['page_title'] = wp_title( '|', false );
            
            $context['footer'] = array();
            $context['footer']['copyright_range'] = cima_copyright_range('2013');
            $context['footer']['newsletter_signup'] = Newsletter_Signup::output_newsletter_form();
            $context['footer']['menu']['col_1'] = new TimberMenu('footer_menu_col1');
            $context['footer']['menu']['col_2'] = new TimberMenu('footer_menu_col2');
            $context['footer']['menu']['col_3'] = new TimberMenu('footer_menu_col3');

            
            $context['current_user'] = new TimberUser();
            $context['logout_url'] = wp_logout_url( get_permalink() );
            $context['profile_url'] = admin_url('profile.php');
            $context['job_postings_url'] = admin_url('edit.php?post_type=job_posting');
            
            return $context;
        }

        // public function current_user(){
        //     $current_user = wp_get_current_user();
        //     if ( 0 == $current_user->ID ) {
        //         return null;
        //     } else {
        //         return $current_user;
        //     }
        // }

        /**
         *  Delegates to parent::get_posts() 
         * @param type $args 
         * @return type
         */
        public static function get_posts($args){
            return parent::get_posts($args);
        }

        public static function get_pagination(){
            return parent::get_pagination();
        }
        /*
        *   Returns Feature Blog Post for Home Page Module
        */
        public static function get_featured_post(){
            $featured_post_query = array(
                'post_per_page' => '1'
            );

            return parent::get_post($featured_post_query);
        }

        public static function get_featured_event(){
            $featured_event_query = array(
                'post_per_page' => '1',
                'post_type' => 'event',
                'meta_query' => array(
                    array(
                        'key' => 'start_date',
                        'value' => date('Y-m-d H:i:s' , strtotime('-1 day')),
                        'compare' => '>=',
                        'type' => 'DATETIME'
                    )
                ),
                'orderby' => 'key',
                'order' => 'DESC'
            );

            return parent::get_post($featured_event_query);
        }

        public static function get_events($limit = null){ 
            
            $events_query = array(
                'post_type' => 'event',
                'meta_query' => array(
                    array(
                        'key' => 'start_date',
                        'value' => date('Y-m-d H:i:s' , strtotime('-1 day')),
                        'compare' => '>=',
                        'type' => 'DATETIME'
                    )
                )
            );

            $events_query['posts_per_page'] = $limit ? $limit : '10';
           
            return parent::get_posts($events_query);
        }

        // public static function get_past_events($months_back, $limit = null){ 
            
        //     $events_query = array(
        //         'post_type' => 'event',
        //         'meta_query' => array(
        //             array(
        //                 'key' => 'start_date',
        //                 'value' =>  self::months_back($months_back),
        //                 'compare' => 'BETWEEN',
        //                 'type' => 'DATETIME'
        //             )
        //         )
        //     );

        //     // $events_query['posts_per_page'] = $limit ? $limit : '10';
           
        //     return parent::get_posts($events_query);
        // }


        public static function get_posts_by_type($post_type, $limit = false, $order = 'DESC', $orderby = 'date', $page = null){
            

            // $page = isset($page) ? $page : 0;

            $custom_query = array(
                'post_type' => $post_type,
                'order'     => $order,
                'orderby'   => $orderby
            );


            if (!$limit) {
                $custom_query['nopaging'] = 'true';
            } else{ 
                $custom_query['posts_per_page'] = intval($limit);
                if ($page) {
                    $custom_query['paged'] = $page;
                }
            }

            

            return parent::get_posts($custom_query);
        }

        static public function build_query_date_from_param($param_month = null){
            // if no month is supplied, return current Year-Month
            if ($param_month === null) return date('Y-m');

            $month = date('m', strtotime($param_month));

            $year = date('Y');
            $current_month = date('m');

            if ($month < $current_month) {
                $year = date('Y', strtotime('+1 year'));
            } else {
                $year = date('Y');
            }
            $last_day_of_month = date('Ymd', mktime(0, 0, 0, intval($month+1), 0, $year));
            return array(
                $year.$month.'01',
                $last_day_of_month
            );
        }
        
        /**
         * Takes an integer for fetching events from n months back, 
         * returns the first and last day of that month as array
         * 
         * @param int $months_back 
         * @return array
         */
        static public function months_back($months_back){

            $year = date('Y');
            $last_month = date('m', strtotime($months_back . " month"));

            $last_day_of_month = date('Ymd', mktime(0, 0, 0, intval($last_month+1), 0, $year));
            return array(
                $year.$last_month.'01',
                $last_day_of_month
            );
        }


        
    }    


}// end if Timber exists
