<?php 

function add_menu_icons_styles(){
  wp_enqueue_style( 'screen', get_template_directory_uri() . '/css/admin-styles.css', array(), CIMA_VERSION_NUMBER, 'all' );
}
add_action( 'admin_head', 'add_menu_icons_styles' );