<?php 

add_action( 'admin_menu', 'my_remove_menu_pages' );
function my_remove_menu_pages() {
    if( !current_user_can( 'manage_options' ) ) {
        remove_menu_page('index.php');      
    }
    remove_menu_page('edit-comments.php'); 
}   

add_action( 'init', 'remove_admin_bar' );
function remove_admin_bar() {

    $user_id = get_current_user_id();

    if ( 1  !=  $user_id ){
        show_admin_bar( false );
    }
}
  

remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );