<?php 
// add_filter( 'nav_menu_css_class', 'namespace_menu_classes', 10, 2 );
// function namespace_menu_classes( $classes , $item ){
//   if ( get_post_type() == 'event' || is_archive( 'event' ) ) {
//     // remove that unwanted classes if it's found
//     $classes = str_replace( 'current_page_parent', '', $classes );
//     // find the url you want and add the class you want
//     if ( $item->url == '/events' ) {
//       $classes = str_replace( 'menu-item', 'menu-item current_page_item', $classes );
//       remove_filter( 'nav_menu_css_class', 'namespace_menu_classes', 10, 2 );
//     }
//   }
//   return $classes;
// }


function add_parent_url_menu_class( $classes = array(), $item = false ) {
  // Get current URL
  $current_url = current_url();

  // Get homepage URL
  $homepage_url = trailingslashit( get_bloginfo( 'url' ) );

  // Exclude 404 and homepage
  if( is_404() or $item->url == $homepage_url )
    return $classes;

  if ( get_post_type() == "event" ) {
    // unset($classes[array_search('current_page_parent',$classes)]);
    if ( isset($item->url) )
      if ( strstr( $current_url, $item->url) )
        $classes[] = 'current-menu-item';
  }

  return $classes;
}

function current_url() {
  // Protocol
  $url = ( 'on' == $_SERVER['HTTPS'] ) ? 'https://' : 'http://';
  $url .= $_SERVER['SERVER_NAME'];

  // Port
  $url .= ( '80' == $_SERVER['SERVER_PORT'] ) ? '' : ':' . $_SERVER['SERVER_PORT'];
  $url .= $_SERVER['REQUEST_URI'];

  return trailingslashit( $url );
}
add_filter( 'nav_menu_css_class', 'add_parent_url_menu_class', 10, 3 );