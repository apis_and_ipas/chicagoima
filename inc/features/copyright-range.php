<?php

function cima_copyright_range($start_copyright_year = "2013") {
    $current_year = (int) date('Y');
    
    // Don't display the range if the end year is the same as the start year, instead just display the year
    if( $start_copyright_year >= $current_year ) {
        return $current_year;
    } else {
        return $start_copyright_year.' - '.$current_year;
    }
}