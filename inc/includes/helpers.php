<?php 

/**
*   Outputs debug info 
*
*   @param $msg - optional (string)  - text output
*   @param $data - required (Object or Array) 
*/
function debug($msg = '', $data, $kill = false){
    if (is_array($data)){
        echo "<pre>"; echo $msg . "\n"; print_r($data); echo "</pre>";
        $kill ? exit() : '';
    } elseif (is_object($data)){
        echo "<pre>"; echo $msg . "\n"; var_dump($data); echo "</pre>";
        $kill ? exit() : '';
    }
}

/**
*   Dump and die
*
*   @param $msg - optional (string)  - text output
*   @param $data - required (Object or Array) 
*/
function dd($data, $kill = true){
    if (is_array($data)){
        echo "<pre>"; print_r($data); echo "</pre>";
        $kill ? exit() : '';
    } elseif (is_object($data)){
        echo "<pre>"; var_dump($data); echo "</pre>";
        $kill ? exit() : '';
    }
}

/**
*   Label helper for custom post types. 
*   Appends an 's' to singular string 
*   is no plural string is passed.
*   
*   @param $singular - required (string)
*   @param $plural   - optional (string) 
*/
function label_inflector( $singular, $plural = '' ) {
    if ( empty( $plural ) )
        $plural = $singular . 's';
 
    return array(
        'name' => $plural,
        'singular_name' => $singular,
        'search_items' => 'Search ' . $plural,
        'all_items' => $plural,
        'edit_item' => 'Edit ' . $plural,
        'add_new_item' => 'Add New ' . $plural,
        'menu_name' => $plural,
        'new_item' => 'New ' . $singular,
        'view_item' => 'View ' . $plural,
        'not_found' => 'No ' . $plural . ' found',
        'not_found_in_trash' => 'No ' . $plural . ' found in Trash',
        'parent_item_colon' => '',
    );
}

/**
*   Time ago in words
*      
*   @param Date $time
*   @return String 
*
*/
function time_ago($time){
   $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
   $lengths = array("60","60","24","7","4.35","12","10");

   $now = time();

   $difference = $now - $time;
   // $tense         = "ago";

   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
       $difference /= $lengths[$j];
   }

   $difference = round($difference);

   if($difference != 1) {
       $periods[$j].= "s";
   }

   return "{$difference} {$periods[$j]}";
}


/**
*  Takes a URL string and prepends it with http:// if it's missing
*  @param string $url
*  @return  string $url
*/
function maybe_prepend_http($url){
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}

/**
 * Takes a string and prepends "mailto:" if missing
 * @param  string $email an email address
 * @return string        an email address with "mailto:"
 */
function maybe_prepend_mailto($email){
    if (!preg_match("/^mailto:/",$email)) {
        $email = "mailto:" . $email;
    }
    return $email;
}

/**
 * Takes a string thats either a URL or email.
 * Ensures each starts with eithet http:// or mailto:
 * @param  string $url 
 * @return string      
 */
function normalize_emails_and_urls($url){
    $url = trim($url);
    if (!$url) return;
    //  Tests if a string contains and email. 
    //  May already be prepended with 'mailto:'
    if(preg_match('/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/si', $url)) {
        return maybe_prepend_mailto($url);
    } else { 
        return maybe_prepend_http($url);
    }
 }