<?php 

class CIMA_Member_Directory{


    function __construct(){
        add_action('wp_ajax_get_members', array($this, 'ajaxHandler'));
        add_action('wp_ajax_nopriv_get_members', array($this, 'ajaxHandler'));

        add_action('wp_ajax_search_members', array($this, 'ajaxSearchHandler'));
        add_action('wp_ajax_nopriv_search_members', array($this, 'ajaxSearchHandler'));
    }


    /**
     * JSON encodes and array and then dies.
     * @param array $data 
     * @return void
     */
    function ajaxResponse($data){
        echo json_encode($data);
        die();
    }

    /**
     * Handles the ajax request and returns either the array of users or an error message to be processed by ajaxResponse method
     * @return array
     */
    function ajaxHandler(){
        
        $per_page = isset($_REQUEST['per_page']) ? $_REQUEST['per_page'] : 5;
        $paged = isset($_REQUEST['offset']) ? $_REQUEST['offset'] : 0;

        // Ensure you can't page more than there are pages
        $users = count_users();
        // dd($users['avail_roles']);
        $user_count = (int) $users['avail_roles']['subscriber']  + (int) $users['avail_roles']['administrator'] ;
        $num_pages =  ceil($user_count / $per_page);
        $num_pages = ($num_pages < 1) ? 1 : $num_pages;

        $data_query = $this->userQuery($per_page, $paged);
        // $return_this = $data_query;
        $user_meta = $this->getUsersMeta($data_query);

        if (!empty($data_query)){
            $response = array(
                'per_page' => $per_page,
                'paged'    => ($paged + 1),
                'num_pages' => $num_pages,
                'data'     => $user_meta
            );

            return $this->ajaxResponse($response);

        } else {
            $no_response = array(
                'error' => 'no users found'
            );
            return $this->ajaxResponse($no_response);
        }
        
    }

    /**
     * Allows for a paginatable user query
     * @param int $per_page 
     * @param int $paged 
     * @return object
     */
    function userQuery($per_page, $paged){
        $args = array(
            'number'  => $per_page,
            'offset'  => $paged,
            // 'role'    => 'Subscriber'
        );
        $query = new WP_User_Query($args);
        return $query->results;
    }

    /**
     * Takes an array of users and returns an array of users with specific metadata
     * @param array $users 
     * @return array
     */
    function getUsersMeta($users){
        $clean_users = array();
        foreach ($users as $user) {
            $clean_users[] = $this->getUserMeta($user);
        }
        return $clean_users;
    }

    /**
     * Gets Basic or Extended Meta for CIMA Members
     * @param obj $user 
     * @param bool $extended 
     * @return obj
     */
    public static function getUserMeta($user, $extended = false){

        $user_id = $user->ID;

        $all_meta_for_user = array_map( function( $a ){ return $a[0]; }, get_user_meta( $user_id ) );

        $clean_user = array();
        $clean_user['ID'] = $user_id;
        $clean_user['display_name'] = $user->display_name;
        $clean_user['company'] = isset( $all_meta_for_user['company'] ) ? esc_html( $all_meta_for_user['company'] ) : null;;
        $clean_user['job_title'] = isset( $all_meta_for_user['job_title'] ) ? esc_html( $all_meta_for_user['job_title'] ) : null;
        $clean_user['registered_date'] = isset( $all_meta_for_user['registered_on'] ) ? esc_html( $all_meta_for_user['registered_on'] ) : null;
        if ($clean_user['registered_date']){
            $clean_user['registered'] = time_ago( strtotime( $clean_user['registered_date'] ) );
        } else {
            $clean_user['registered'] = time_ago( strtotime( $user->user_registered ) );
        }
        
        $membership_level = pmpro_getMembershipLevelForUser( $user_id );
        $clean_user['membership_level'] = isset($membership_level->name) ? $membership_level->name : null;; 

        $hs = isset($all_meta_for_user['headshot']) ? $all_meta_for_user['headshot'] : null;
        if ($hs){
            $headshot_src = wp_get_attachment_image_src( $hs,'full' );
            $clean_user['headshot'] = $headshot_src[0];
        } else {
            $clean_user['headshot'] = null;
        }

     

        if ($extended){
            $clean_user['bio'] = esc_html( $all_meta_for_user['description'] );
            $clean_user['email'] = esc_html( $user->user_email );
            $clean_user['twitter_name'] = isset( $all_meta_for_user['twitter_name'] ) ? esc_html( $all_meta_for_user['twitter_name'] ) : null;
            $clean_user['linkedin_profile'] = isset( $all_meta_for_user['linkedin_profile'] ) ? esc_html( normalize_emails_and_urls( $all_meta_for_user['linkedin_profile'] ) ) : null;
        }

        return $clean_user;
    }

}


global $member_directory;
$member_directory = new CIMA_Member_Directory;