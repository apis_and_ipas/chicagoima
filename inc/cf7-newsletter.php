<?php 
require_once 'et_client.php';

class ContactForm_Signup {


    function __construct(){
        add_action( 'wpcf7_before_send_mail', array( $this, 'contact_form_signup' ));
    } 

    /**
     *  Handles Contact Form Submission Hook
     *  All CF7 forms with newsletter checked will be signed up for the Exact Target Mailing List.
     *  @param  [type] $cf7 [description]
     *  @return [type]      [description]
     */
    public function contact_form_signup($cf7){
        if ( isset($cf7->posted_data['newsletter']) && ( $cf7->posted_data['newsletter'] ===  "true") ) {
            $email = $cf7->posted_data['email'];
            $response = ET_Client::subscribe($email);
            // die($response);
        }
        return $cf7;
    }


}


// initialize!
global $contactForm_signup;
$contactForm_signup = new ContactForm_Signup;
