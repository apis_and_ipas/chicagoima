<?php 

require_once(dirname( __FILE__ ) .'/lib/facebook-php-sdk/src/facebook.php');

class CIMA_Gallery {

    public $facebook;
    public $env;
    public $app_access_token;
    public $albums;
    public $images;
    public $exclude = array(
        '171427899580535', 
        '324631817593475', 
        '576508749072446', 
        '118481998208459', 
        '494963170560338',
        '635248633198457'  
    );
    public $fb_apps = array(
        'development' => array(
          'appId'  => '1424934687736476',
          'secret' => '1da17fac7ddc98e4430345139702238e',
        ),
        'staging' => array(
          'appId'  => '1393450930922066',
          'secret' => 'd3595f61f45d1d5c7ab21431b03333e0',
        ),
        'production' => array(
          'appId'  => '1552804378277568',
          'secret' => '987a1eb4d1bfc22ec2b4eafd0395e7c8',
        )
    );
    public $cache_time = DAY_IN_SECONDS ;

    function __construct(){
        $this->set_env();
        $this->facebook = new Facebook($this->fb_apps[$this->env]);
        $this->app_access_token = $this->facebook->getApplicationAccessToken();
    }

    public function set_env(){
        // if ('http://cima.dev' == ){
        //     $this->env = 'staging';
        // } 
        switch ($_SERVER['HTTP_HOST']) {
            case 'http://cima.dev':
                $this->env = 'development';
                break;

            case 'http://cima.doejoapp.com':
                $this->env = 'staging';
                break;
            
            default:
                $this->env = 'production';
                break;
        }
    }
  
    /**
    *      Fetches object of images from transient cache OR from Graph API
    */
    public function get_albums(){
        $this->albums = get_transient( 'cima_fb_albums' );

        if (false == $this->albums){

            $response = $this->facebook->api("/ChicagoIMA/albums");
            $data = $response['data'];
            $albums = array_filter($response['data'], array($this, 'to_exclude') );

            foreach ($albums as $album) {
                $cover_id = $album['cover_photo'];
                $cover_request = $this->facebook->api("/$cover_id");
                $this->albums[$album['id']] = array(
                    'id'            => $album['id'],
                    'name'          => $album['name'],
                    'cover_photo'   => $cover_request['source'],
                    'images'        => array()
                );
                if (isset($album['location'])) {
                    $this->albums[$album['id']]['location'] = $album['location'];
                } 
            }

            $this->update_albums();
        }
        return $this->albums;
    }

    public function update_albums($album = null) {
        if ($album && $album['id']){
            $this->albums[$album['id']] = $album;
        } 
        set_transient( 'cima_fb_albums', $this->albums, $this->cache_time );
    }


    public function get_album($album_id){
        // Grab Albums object, this will hit transients cache if available;
        $albums = $this->get_albums();

        $album = $this->albums[$album_id];
        // $album = array_search( $album_id , $albums );

        $album['images'] = $this->get_album_images($album_id);

        // Update albums object
        $this->update_albums($album);


        return $album;
    }

    public function get_album_images($album_id){
        
        $response = $this->facebook->api("/$album_id/photos");
        $data = $response['data'];
        $images = array();
        foreach ($data as $image) {
            $images[] = array(
                'image_id' => $image['id'],
                'url' => $image['source']
            );
        }
           
        return $images;
    }   


    /**
    *   Filters out albums based on the 'exclude' property.
    *   @param Array 
    */
    public function to_exclude($data){
        return !in_array($data['id'], $this->exclude);
    }

    
}

global $cima_gallery ;
$cima_gallery = new CIMA_Gallery;