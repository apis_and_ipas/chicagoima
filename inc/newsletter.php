<?php 
require_once 'et_client.php';

class Newsletter_Signup {

    function __construct(){
        add_action('init', array( $this,'register_script' ) ); 
        add_action('init', array( $this,'ajax_setup' ) );
    } 

    public function register_script(){
        wp_enqueue_script( 'cima-exact-target', get_template_directory_uri().'/js/newsletter.js' , array('jquery'), CIMA_VERSION_NUMBER, true);
    }

    public function ajax_setup(){
        add_action('wp_ajax_create_subscriber', array($this, 'subscribe'));
        add_action('wp_ajax_nopriv_create_subscriber', array($this, 'subscribe'));
    }

    public function subscribe(){
        if ( !empty($_POST['data']['subscriber_email']) ) {
            $email = $_POST['data']['subscriber_email'];
            $response = ET_Client::subscribe($email);
            die($response);
        }
    }

    public function output_newsletter_form(){
        ob_start();?>

        <div class="form-wrapper cima-form">
            <form id="newsletter-signup" parsley-validate>
              <div><label for="subscriber_email">Email Address</label></div>
              <div><input type="text" name="subscriber_email" class="newsletter" id="subscriber_email" parsley-type="email" parsley-required="true"/></div>
              <div><input type="submit" value="Submit" /></div>
            </form>
        </div>

        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

}


// initialize!
global $newletter_signup;
$newletter_signup = new Newsletter_Signup;