<?php


class CIMA_Verify {


    function __construct(){

        add_action('wp_ajax_verify_student', array($this, 'verify_student'));
        add_action('wp_ajax_nopriv_verify_student', array($this, 'verify_student'));

        add_action('wp_ajax_verify_agency', array($this, 'verify_agency'));
        add_action('wp_ajax_nopriv_verify_agency', array($this, 'verify_agency'));
    
    }

    /**
     * JSON encodes and array and then dies.
     * @param array $data 
     * @return void
     */
    function ajax_response($data){
        echo json_encode($data);
        die();
    }

   
    public function verify_student(){
        $email = isset($_REQUEST['email']) ? htmlspecialchars($_REQUEST['email']) : null;

        $valid = $this->check_validity($email, 'student');

        $response = array(
            'value' => $email,
            'type' => 'student',
            'verified' => $valid
        );

        return $this->ajax_response($response);
    }

    public function verify_agency(){

        $email = isset( $_REQUEST['email'] ) ? htmlspecialchars( $_REQUEST['email'] ) : null;

        $valid = $this->check_validity($email, 'agency');

        $response = array(
            'value' => $email,
            'type' => 'agency',
            'verified' => $valid
        );

        return $this->ajax_response($response);
    }

    /**
     *  @param string $email  -an email address
     *  @param string $type - type of validation, agency or student
     *  @return  bool if the email provided's domain in the whitelist type supplied?
     */
    public static function check_validity($email, $type){
        if (!$type) throw new Exception('$type is needed!');

        $whitelist_raw = null;

        if ($type === 'agency'){
            $whitelist_raw = get_field('agency_corporate_whitelist', 'option');
        } elseif ($type === 'student'){
            $whitelist_raw = get_field('student_educator_whitelist', 'option');
        } else {
            throw new Exception('Something went wrong!');
        }
        // dd($whitelist_raw);
        $email_domain = self::get_email_domain($email);
        $whitelist = explode(', ', $whitelist_raw);

        // return $whitelist;
        return in_array($email_domain, $whitelist);

    }

    /**
     *  @param string  $email an email address
     *  @return  string the domain of the provided email address
     */
    public static function get_email_domain($email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)){
            $email = trim($email);
            return array_pop( explode( '@', $email ) );
        }
    }


}

global $cima_verify;
$cima_verify =  new CIMA_Verify();