<?php 

// Uncommeny to flush the cache
// delete_transient( 'job_city' );
// delete_transient( 'job_job_type' );
// delete_transient( 'job_category' );


class CIMA_Jobs {

    //  Set to false to turn on cacheing
    public static $debug = false;

    public static function get_jobs_meta($field){

          $meta_key = 'job_' . $field;

          // fetch cached arrays, if not in debug mode.
          $fields = self::$debug ? false : get_transient( $meta_key );

          // If nothing was returned from cache, query the db
          if (!$fields) {
              $fields_query = new WP_Query(array(
                  'post_type' => 'job_posting',
                  'no_paging' => 'true',
                  'status' => 'publish',
                  'meta_key' => $field
              ));

              $tmp = array();

            // Build and array of all the meta values.
            if ($fields_query->have_posts()): while($fields_query->have_posts()): $fields_query->the_post();
                $tmp[] = get_field($field);
            endwhile; endif;

            // Ensure unique values
            $fields = array_unique($tmp);
            $tmp = null;

            // Cache the array if not in debug mode.
            if (!self::$debug) set_transient( $meta_key, $fields, 12 * HOUR_IN_SECONDS );

          }//endif

          
          return $fields;
    }

}


