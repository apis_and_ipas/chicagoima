# CIMA - Chicago Interactive Marketing Association

## Dependencies
  * node.js and npm
  * Various Grunt plugins and modules as defined in package.json
  * the following Wordpress Plugins
    * Advanced Custom Fields
    * Advanced Custom Fields: Repeater Field
    * Advanced Custom Fields: Date and Time Picker
    * Contact Form 7
    * Contact Form DB
    * Duplicate Post
    * iThemes Security
    * Eventbrite for WordPress (custom fork) available on server
    * Map Cap
    * Metronet Reorder Posts
    * Paid Memberships Pro (custom fork) available on server
    * Single Value Taxonomy UI
    * Timber
    * TinyMCE Advanced
    * WP-Mail-SMTP
    * optionally, these were used in development
      * Debug Bar
      * Import Users from CSV
      * PMPro Import Users from CSV
      * Rewrite Rules Inspector


## Setup 
  1. Typical Wordpress Theme setup. Move project into /wp-content/themes
  * `cd` into the theme folder and run  `npm install`, which will fetch development dependencies for Grunt and rsync tasks

## Timber
The first thing you'll notice about this theme is the use of twig templates, through the use of [Timber](http://upstatement.com/timber/) plugin.
You'll need to refer to these docs when working on the templates.

## Grunt
This project uses Grunt for several tasks as defined in Gruntfile.js and further in rsync-config.json.
  * `default` or `watch`: Watch for changes to twig, scss, and js files and do a live-reload. Lints JS files.
  * `build`: compiles SCSS files in 'compressed' mode for production.
  * `deploy-staging`: uses rsync to sync of theme, uploads and plugins to staging site: cima.doejoapp.com

## Database
A database dump of the production servers can be had by using the backup interface provided by the iThemes security plugin