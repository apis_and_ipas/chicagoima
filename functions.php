<?php 

define('CIMA_VERSION_NUMBER', '0.0.3'); //Used for cache-busting

require_once dirname(__FILE__). '/inc/bootstrap.php';

 /**
 * General configuration for the theme
 */
  add_action( 'after_setup_theme', 'cima_setup' );
 function cima_setup(){

    // ———————————————————————————————-
    // Set image sizes
    // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
    // http://codex.wordpress.org/Function_Reference/add_image_size
    // ———————————————————————————————-
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 200, 150, true ); // Default thumbnail size
    add_image_size( 'User Headshot', 125, 125); // Headshot for 'The Author' sidebar
 }


 /**
 * Enqueue theme stylesheet files
 *
 * http://codex.wordpress.org/Function_Reference/wp_enqueue_style
 */
add_action( 'wp_enqueue_scripts', 'cima_enqueue_styles' );
function cima_enqueue_styles() {
    wp_enqueue_style( 'screen', get_template_directory_uri() . '/css/screen.css', array(), CIMA_VERSION_NUMBER, 'all' );

    if (is_page('gallery')) {
        wp_enqueue_style( 'lightbox-css', get_template_directory_uri() . '/css/lightbox.css', array(), CIMA_VERSION_NUMBER, 'all' );
    }
}


/**
 * Enqueue theme Javascript files
 *
 * http://codex.wordpress.org/Function_Reference/wp_enqueue_script
 */
add_action( 'wp_enqueue_scripts', 'cima_enqueue_scripts' );
function cima_enqueue_scripts() {
    wp_enqueue_script( 'parsley', get_template_directory_uri() . '/js/lib/parsley.min.js', array('jquery'), CIMA_VERSION_NUMBER,  true );
    wp_enqueue_script( 'modernizr', '//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js', array(), CIMA_VERSION_NUMBER);
    wp_enqueue_script( 'mobile-nav', get_template_directory_uri() . '/js/lib/mobile-nav.js', array('jquery'), CIMA_VERSION_NUMBER,  true );
    wp_enqueue_script( 'events-module', get_template_directory_uri() . '/js/lib/events-module.js', array('jquery'), CIMA_VERSION_NUMBER,  true );
    wp_enqueue_script( 'reveal', get_template_directory_uri() . '/js/lib/jquery.reveal.js', array('jquery'), CIMA_VERSION_NUMBER,  true );
    wp_enqueue_script( 'info-toggler', get_template_directory_uri() . '/js/lib/jquery.info-toggler.js', array('jquery'), CIMA_VERSION_NUMBER,  true );
    wp_enqueue_script( 'equal-heights', get_template_directory_uri() . '/js/lib/jquery.equal-heights.js', array('jquery'), CIMA_VERSION_NUMBER,  true );
    wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array('jquery', 'mobile-nav'), CIMA_VERSION_NUMBER,  true );
   
    if (is_page('member-directory')){
        wp_enqueue_script( 'pager', get_template_directory_uri() . '/js/lib/pager.js', array(), CIMA_VERSION_NUMBER, false );
        wp_enqueue_script( 'member-directory', get_template_directory_uri() . '/js/member_directory.js', array('pager', 'jquery'), CIMA_VERSION_NUMBER, true );
    }


    if ( is_page('volunteer') || is_page('speak') || is_page('sponsor') || is_page('join-the-board') || is_page('send-us-a-message') ) {
        wp_enqueue_script( 'form-valiation', get_template_directory_uri() . '/js/form-validation.js', array('jquery','parsley'), CIMA_VERSION_NUMBER,  true );
    }

    if (is_post_type_archive( 'kc-post' )){
        wp_enqueue_script('kc-post-toggle', get_template_directory_uri() . '/js/kc-toggler.js', array('jquery'), CIMA_VERSION_NUMBER,  true );
    }

    if (is_page('gallery')) {
        wp_enqueue_script('underscore');
        wp_enqueue_script('lightbox', get_template_directory_uri() . '/js/lib/lightbox.js', array('jquery'), CIMA_VERSION_NUMBER,  true );
        wp_enqueue_script('gallery', get_template_directory_uri() . '/js/gallery.js', array('jquery'), CIMA_VERSION_NUMBER,  true );
    }
    if (is_page('membership-checkout'))
        wp_enqueue_script( 'member-verify', get_template_directory_uri() . '/js/member_verification.js', array('jquery'), CIMA_VERSION_NUMBER,  true );
}

/**
 *  Register Navigation Menus
 *
 */
add_action( 'init', 'navigation_menus' );
function navigation_menus() {

    $locations = array(
        'header_menu' => __( 'Header Menu', 'cima-theme' ),
        'footer_menu_col1' => __( 'Footer Menu Column 1', 'cima-theme' ),
        'footer_menu_col2' => __( 'Footer Menu Column 2', 'cima-theme' ),
        'footer_menu_col3' => __( 'Footer Menu Column 3', 'cima-theme' ),
    );
    register_nav_menus( $locations );
}

function is_login_page() {
    return in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'));
}



// / Add a custom controller
add_filter('json_api_controllers', 'add_member_directory_controller');
function add_member_directory_controller($controllers) {
  // Corresponds to the class JSON_API_MyController_Controller
  $controllers[] = 'member_directory';
  return $controllers;
}

// Register the source file for JSON_API_Widgets_Controller
add_filter('json_api_member_directory_controller_path', 'member_directory_controller_path');
function member_directory_controller_path($default_path) {
  return get_stylesheet_directory() . '/inc/cima-member-directory-json.php';
}


/** changing default wordpres email settings */
 
add_filter('wp_mail_from', 'new_mail_from');
add_filter('wp_mail_from_name', 'new_mail_from_name');

function new_mail_from($old) {
    return 'no-reply@chicagoima.org';
}
function new_mail_from_name($old) {
    return 'CIMA Contact Form';
}



function my_acf_options_page_settings( $settings )
{
    $settings['pages'] = array('Membership Verification');
 
    return $settings;
}
 
add_filter('acf/options_page/settings', 'my_acf_options_page_settings');



add_filter( 'metronet_reorder_post_types', 'slug_set_reorder' );
function slug_set_reorder( $post_types ) {
    if ( current_user_can('edit_users') ) {
        $post_types = array( 'post', 'board-member' );
    } else {
        $post_types = array( 'post');
    }
    return $post_types;
}


function get_last_order_date($user_id = NULL, $order_status = "success"){

    global $wpdb, $tableprefix, $current_user;

    get_currentuserinfo();

    if(!$user_id) {
        $user_id = $current_user->ID;
    }

    if(!$user_id) {
        return false;
    }

    //get last order
    $order = new MemberOrder();
    $order->getLastMemberOrder($user_id, $order_status);
    
    //get current membership level - Not used??
    // $level = pmpro_getMembershipLevelForUser($user_id);     

    $nextdate = null;

    if ( !empty($order) && !empty($order->timestamp) ) {
 
        $lastdate = date("Y-m-d", $order->timestamp);

        $nextdate = strtotime('+1 year', strtotime( $lastdate ) );

    } 

    return $nextdate;

}






