<?php
if (!class_exists('Timber')){
  echo 'Timber not activated';
}

$context = CIMA::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['pagination'] = array();
$context['pagination']['all'] = site_url( 'blog' );
$context['pagination']['next'] = get_permalink( get_adjacent_post(false,'',true)->ID );



$author_id = $post->post_author;
$headshot_id = get_the_author_meta( 'headshot', $author_id );

$context['author_meta'] = array();
$context['author_meta']['ID'] = $author_id;
$context['author_meta']['headshot'] = wp_get_attachment_image( $headshot_id , 'User Headshot' );
$context['author_meta']['name'] = get_the_author_meta( 'display_name', $author_id );
$context['author_meta']['job_title'] = get_the_author_meta( 'job_title', $author_id );
$context['author_meta']['bio'] = get_the_author_meta( 'description', $author_id );


CIMA::render('single-post.twig', $context);