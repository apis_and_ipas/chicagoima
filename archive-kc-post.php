<?php
if (!class_exists('Timber')){
  echo 'Timber not activated';
}
global $numpages;
$page = isset( $_REQUEST['page'] ) ? sanitize_text_field( $_REQUEST['page'] ) : 0;
$posts_per_page = "10";
$args = array(
    'post_type' => 'kc-post',
    'posts_per_page' => $posts_per_page,
    'paged' => $page
);

$total =  wp_count_posts( 'kc-post' )->publish;
$total_pages = ($total /  intval($posts_per_page));

$context = CIMA::get_context();
$context['posts'] = CIMA::get_posts($args);

$prev_link = null;
$next_link = null;

/**
*  Found out way too late into the project that Timber Pagination only works 
*   well in the blog section and not really at all in archive or custom posts types.
*   The following code works, I think.
*/
// Dont let it page below 0
if ($page > 0){
    $prev_link = array(  'link' => '/knowledge-center/?page=' . ($page - 1 ) );
} 
// Don't let it page beyond the total number of pages
if ($page <= $total_pages && (false == ($page + 2 ) > $total_pages)) {
  $next_link = array( 'link' => '/knowledge-center/?page=' . ($page + 1) );
}

$context['pagination'] = array(
  'prev' => $prev_link,
  'next' => $next_link,
);

CIMA::render('knowledge-center.twig', $context);





