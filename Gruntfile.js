module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        rsync: grunt.file.readJSON('rsync-config.json'),
        sass: {
            dist: {
                options: {
                    style: 'compressed'

                },
                files: {
                    './css/screen.css': './src/scss/screen.scss',
                    './css/admin-styles.css': './src/scss/admin-styles.scss'
                }
            },
            dev: {
                options: {
                    style: 'nested'
                },
                files: {
                    './css/screen.css': './src/scss/screen.scss',
                    './css/admin-styles.css': './src/scss/admin-styles.scss'
                }
            }
        },
        jshint: {
            options: {
                browser: true,
                    '-W030': true,
                globals: {
                    jQuery: true
                },
            },
            all: ['./src/js/**/*.js', 'Gruntfile.js']
        },
        watch: {
            options: {
                livereload: true,
            },
            twig: {
                files: ['views/**/*.twig']
            },
            scss: {
                files: ['**/*.scss'],
                tasks: ['sass:dev']
            },
            scripts: {
                files: ['./js/**/*.js', 'Gruntfile.js'],
                tasks: ['jshint']
            }
        }
    });

    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.registerTask('default', ['watch']);
    grunt.registerTask('build', ['sass:dist']);
    grunt.registerTask('deploy-staging', ['rsync:stage','rsync:stage_plugins','rsync:stage_uploads']);


};