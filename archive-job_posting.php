<?php
if (!class_exists('Timber')){
  echo 'Timber not activated';
}

$context = CIMA::get_context();

$paged = ( get_query_var('paged') ) ? get_query_var( 'paged' ) : 1;

$posts_per_page = 10;
$context['posts_per_page'] = $posts_per_page;



$context['job_locations'] = CIMA_Jobs::get_jobs_meta('city');
$context['job_category'] =  CIMA_Jobs::get_jobs_meta('category');
$context['job_types'] = CIMA_Jobs::get_jobs_meta('job_type');

$job_type =     isset($_REQUEST['job_type']) ? $_REQUEST['job_type'] : false;
$job_category = isset($_REQUEST['job_category']) ? $_REQUEST['job_category'] : false;
$job_city =     isset($_REQUEST['job_city']) ? $_REQUEST['job_city'] : false;



if ($job_type || $job_category || $job_city){
    $args = array(
      'post_type'      => 'job_posting',
      'post_status'    => 'publish',
      'posts_per_page' => $posts_per_page,
      'paged'          => $paged,
      'meta_query'     => array(
          'relation' => 'AND'
       )
    );

    if ($job_type !== 'any')
        $args['meta_query'][] = array(
            'key' => 'job_type',
            'value' => $job_type,
            'compare' => 'LIKE',
        );
    if ($job_category !== 'any')
        $args['meta_query'][] = array(
            'key' => 'category',
            'value' => $job_category,
            'compare' => 'LIKE',
        );
    if ($job_city !== 'any')
        $args['meta_query'][] = array(
            'key' => 'city',
            'value' => $job_city,
            'compare' => 'LIKE',
        );

    $context['section_title'] = 'Results';
    // $context['jobs'] = CIMA::get_posts($args);
    // 
    $context['jobs'] = CIMA::get_posts($args);
    $context['pagination'] = CIMA::get_pagination();

    $context['current_job_type'] = $job_type;
    $context['current_job_category'] = $job_category;
    $context['current_job_city'] = $job_city;



} else {
    $context['section_title'] = 'Recent Listings';
    $args = array(
      'post_type'      => 'job_posting',
      'post_status'    => 'publish',
      'posts_per_page' => $posts_per_page,
      'paged'          => $paged,
    );
    $context['jobs'] = CIMA::get_posts($args);
    $context['pagination'] = CIMA::get_pagination();

    // Queries job_postings so only available job criteria appears in the select forms
} 



$context['footer_content_left'] = get_field('footer_content_left', 13);
$context['footer_content_right'] = get_field('footer_content_right', 13);
$context['footer_content_right_signed_in'] = get_field('footer_content_right_signed_in', 13);
$context['one_week_ago'] = strtotime('-1 week');




CIMA::render('page-job-board.twig', $context);