<?php
if (!class_exists('Timber')){
  echo 'Timber not activated';
}

$context= CIMA::get_context();
$context['post'] = CIMA::get_post();
$posts = CIMA::get_posts('TimberPost');
$context['posts'] = $posts;

$context['modules'] = array();

$context['modules']['featured_post'] = CIMA::get_featured_post();
$context['modules']['featured_event'] = CIMA::get_featured_event();

// Grab 5 jobs and pick one randomly.
$jobs = CIMA::get_posts_by_type('job_posting', 5);
$featured = array_rand($jobs, 1);
$context['modules']['featured_job'] = $jobs[$featured];

$context['modules']['recent_jobs'] = CIMA::get_posts_by_type('job_posting', 5);
$context['upcoming_events'] = CIMA::get_events(4);

$context['get_involved'] = get_field('get_involved_module_text');
$context['board_members'] = get_field('board_members_module_text');

CIMA::render('home.twig', $context);