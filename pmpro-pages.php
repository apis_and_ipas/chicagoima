<?php
/**
 * Template Name: PMProPages
 */
global $wpdb, $pmpro_msg, $pmpro_msgt, $pmpro_levels, $current_user, $pmpro_currency_symbol;

$context = CIMA::get_context();
$post = new TimberPost();
$context['post'] = $post;


/// Load extra page content
switch ($post->post_name) {

  case 'membership-levels':
    $context['levels'] = $pmpro_levels;
    break;

  // case 'membership-checkout':
  //   global $username, $password, $password2, $bfirstname, $blastname, $baddress1;
  //   global $baddress2, $bcity, $bstate, $bzipcode, $bcountry, $bphone, $bemail;
  //   global $bconfirmemail, $CardType, $AccountNumber, $ExpirationMonth, $ExpirationYear; 
    
  //   // Grab option string and make it an array
  //   $pmpro_accepted_credit_cards = pmpro_getOption("accepted_credit_cards");
  //   $pmpro_accepted_credit_cards = explode(",", $pmpro_accepted_credit_cards);
  //   $CVV = !empty($_REQUEST['CVV']) ? $_REQUEST['CVV'] : null;
  //   $context['pmpro'] = array(
  //       'debug' => $_POST,
  //       'level' => $pmpro_level,
  //       'levels' => $pmpro_levels,
  //       'msg' => $pmpro_msg,
  //       'review' => $pmpro_review,
  //       'gateway' => $gateway,
  //       'util' => array(
  //           'states' => $pmpro_states_abbreviations,
  //           'accepted_credit_cards' => $pmpro_accepted_credit_cards,
  //           'state_abbrs' => $pmpro_states_abbreviations,
  //           'cc_years' => range(date('Y'), date('Y') + 10 , 1),
  //           'braintree_encryptionkey' => pmpro_getOption("braintree_encryptionkey")
  //       ),
  //       'user' => array(
  //           'username'   =>  $username,
  //           'password'   =>  $password,
  //           'password2'  =>  $password2,
  //           'email'      =>  $bemail,
  //           'confirmemail'  =>  $bconfirmemail,
  //           'firstname' =>  $bfirstname,
  //           'lastname'  =>  $blastname,
  //           'address1'  =>  $baddress1,
  //           'address2'  =>  $baddress2,
  //           'city'      =>  $bcity,
  //           'state'     =>  $bstate,
  //           'zipcode'   =>  $bzipcode,
  //           'country'   =>  $bcountry,
  //           'phone'     =>  $bphone,
  //           'CardType'         =>  $CardType,
  //           'AccountNumber'    =>  $AccountNumber,
  //           'ExpirationMonth'  =>  $ExpirationMonth,
  //           'ExpirationYear'   =>  $ExpirationYear,
  //           'CVV' => $CVV
  //       )
  //   );
  //   break;   

  default:
    # code...
    break;
}

CIMA::render(array('page-' . $post->post_name . '.twig'), $context);