<?php
if (!class_exists('Timber')){
  echo 'Timber not activated';
}

$context = CIMA::get_context();
$job = new TimberPost();
$context['job'] = $job;
$context['job_application_link'] = normalize_emails_and_urls($job->link_to_application);
 
CIMA::render('single-job.twig', $context);
