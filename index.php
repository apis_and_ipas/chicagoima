<?php
if (!class_exists('Timber')){
  echo 'Timber not activated';
}

$context = CIMA::get_context();
$posts = CIMA::get_posts('TimberPost');
$context['posts'] = $posts;
$context['pagination'] = Timber::get_pagination();

CIMA::render('blog.twig', $context);