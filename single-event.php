<?php
if (!class_exists('Timber')){
  echo 'Timber not activated';
}

$context = CIMA::get_context();
$event = new TimberPost();
$context['event'] = $event;
// $context['splash_image'] = 
$context['event_access_code'] = get_field('member_ticket_access_code');

// dd($event);

if ($event->ticket){
    // Unserialize all ticket data
    $context['event_tickets'] = array(); 
    
    $src = get_post_meta( $event->ID, 'map_embed', true );
    if ($src){
        preg_match('|<iframe [^>]*src="([^"]+")[^>]*|', $src, $matches);
        $context['event_map_src'] = $matches[1];
    }
    
    if (is_array($event->ticket)) {
        // If there is more than one ticket...
        foreach ($event->ticket as $ticket) {
            // Double serialized? If you say so..
            $context['event_tickets'][] = unserialize( unserialize($ticket) );
        }

    } else {
        // If theres only one ticket
        $context['event_tickets'][] = unserialize( $event->ticket );
    }
}



$event_meta = array('speakers', 'sponsors');

// Takes each event meta type (Speakers, Sponsors, etc.)
// adds all meta data to the context object
foreach ($event_meta as $meta_type) {
    $context[$meta_type] = array();
    $meta_data = get_field($meta_type, $event->ID);

    if ($meta_data){
        foreach ($meta_data as $meta_item) {
            $context[$meta_type][] = new TimberPost($meta_item);
        } 
    }
} 

CIMA::render('single-event.twig', $context);
