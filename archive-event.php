<?php
if (!class_exists('Timber')){
  echo 'Timber not activated';
}

// Just to try up the markup in the template
$months_array = array(
  'jan' => 'January', 
  'feb' => 'February', 
  'mar' => 'March', 
  'apr' => 'April', 
  'may' => 'May', 
  'jun' => 'June', 
  'jul' => 'July', 
  'aug' => 'August', 
  'sep' => 'September', 
  'oct' => 'October', 
  'nov' => 'November', 
  'dec' => 'December'
);

$context = CIMA::get_context();
$context['featured_event'] = CIMA::get_featured_event();
$context['months'] = $months_array;
$context['current_month'] = strtolower(date('M'));
/**
*  Month Filtering
*/
if (isset($_REQUEST['event_month'])){
  
  $query_date = sanitize_text_field($_REQUEST['event_month']);
  $context['query_date'] = $query_date;

  $args = array(
    'post_type'   => 'event',
    'nopaging'    => 'true',
    'meta_query'  => array(
      array(
        'key'     => 'start_date',
        'value'   => CIMA::build_query_date_from_param($query_date),
        'compare' => 'BETWEEN',
        'type'    => 'DATE'
      )
    )
  );

  $context['events'] = CIMA::get_posts($args);
} else {
  $context['events'] = CIMA::get_events();
}

CIMA::render('events.twig', $context);





