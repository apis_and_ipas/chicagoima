<?php
if (!class_exists('Timber')){
  echo 'Timber not activated';
}

// $context = CIMA::get_context();

// $page = isset( $_REQUEST['pg'] ) ? esc_attr( $_REQUEST['pg'] ) : 1;
// $posts_per_page = 2;

// $post = new TimberPost();
// $context['post'] = $post;

// $context['job_locations'] = CIMA_Jobs::get_jobs_meta('city');
// $context['job_category'] =  CIMA_Jobs::get_jobs_meta('category');
// $context['job_types'] = CIMA_Jobs::get_jobs_meta('job_type');

// $job_type =     isset($_REQUEST['job_type']) ? $_REQUEST['job_type'] : false;
// $job_category = isset($_REQUEST['job_category']) ? $_REQUEST['job_category'] : false;
// $job_city =     isset($_REQUEST['job_city']) ? $_REQUEST['job_city'] : false;


// // $prev_link = null;
// // $next_link = null;

// /**
// *  Found out way too late into the project that Timber Pagination only works 
// *   well in the blog section and not really at all in archive or custom posts types.
// *   The following code works, I think.
// */

// // $total =  wp_count_posts( 'job_posting' )->publish;
// // $total_pages = ($total /  intval($posts_per_page));

// // // Dont let it page below 0
// // if ($page > 1){
// //     $prev_link = array(  'link' => '/job-board/?pg=' . ($page - 1 ) );
// // } 

// // // Don't let it page beyond the total number of pages
// // if ($page <= $total_pages && (false == ($page + 2 ) > $total_pages)) {
// //   $next_link = array( 'link' => '/job-board/?pg=' . ($page + 1) );
// // }



// if ($job_type || $job_category || $job_city){
//     $args = array(
//       'post_type'      => 'job_posting',
//       'post_status'    => 'publish',
//       'posts_per_page' => $posts_per_page,
//       'paged'          => $page,
//       'meta_query'     => array(
//           'relation' => 'AND'
//        )
//     );

//     if ($job_type !== 'any')
//         $args['meta_query'][] = array(
//             'key' => 'job_type',
//             'value' => $job_type,
//             'compare' => 'LIKE',
//         );
//     if ($job_category !== 'any')
//         $args['meta_query'][] = array(
//             'key' => 'category',
//             'value' => $job_category,
//             'compare' => 'LIKE',
//         );
//     if ($job_city !== 'any')
//         $args['meta_query'][] = array(
//             'key' => 'city',
//             'value' => $job_city,
//             'compare' => 'LIKE',
//         );

//     $context['section_title'] = 'Results';
//     $context['jobs'] = CIMA::get_posts($args);
//     $context['current_job_type'] = $job_type;
//     $context['current_job_category'] = $job_category;
//     $context['current_job_city'] = $job_city;



// } else {
//     $context['section_title'] = 'Recent Listings';
//     $context['jobs'] = CIMA::get_posts_by_type('job_posting', $posts_per_page, 'DESC', 'date', $page);

//     // Queries job_postings so only available job criteria appears in the select forms
// } 

// // $context['qs'] = $_SERVER['QUERY_STRING'];

// // $context['pagination'] = array(
// //   'prev' => $prev_link,
// //   'next' => $next_link,
// // );


// $context['one_week_ago'] = strtotime('-1 week');




// CIMA::render('page-job-board.twig', $context);