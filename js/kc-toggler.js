(function($){

    // log('KCToggler!');

    var $kc_post = $('.kc-post');

    $kc_post.click(function(e){
        e.preventDefault();

        var $this = $(this);
            $content = $this.find('section.content');
            
        // log('clicked!');

        // Remove currently opened
        $('.kc-post.open').removeClass('open');

        // Toggle Class
        $this.toggleClass('open');
        $content.slideToggle(500);
    });

    

})(jQuery);