(function($){

        function _debug(msg){
            if (memberDirectory.config.debug) log(msg);
        }

        window.memberDirectory = {
            cache: {},
            config : {
                debug: false,
                per_page: '10'
            },
            ui:{
                memberTmpl: $('#memberTmpl').html(),
                $pagination: $('.pagination'),
                $prevBtn: $('.prevPage'),
                $nextBtn: $('.nextPage'),
                $outlet: $('#member-container'),
                loader: $('#loader'),
                $form: $('#md_search'),
                setBtnState: function(){
                    _debug('setting btn state');

                    if (pager.getPageLimit() === 1){
                        memberDirectory.ui.$prevBtn.hide();
                        memberDirectory.ui.$nextBtn.hide();
                        _debug('hide');
                    }

                    if (pager.getPage() === 1){
                        _debug('1st page!');
                        memberDirectory.ui.$prevBtn.hide();
                    } else {
                        memberDirectory.ui.$prevBtn.show();
                    }

                    if ((pager.getPage()) === pager.getPageLimit()) {
                        _debug('end of the line');
                        memberDirectory.ui.$nextBtn.hide();
                    } else {
                        memberDirectory.ui.$nextBtn.show();
                    }

                }
            },
            init: function(config) {
                // extend defaults
                $.extend(memberDirectory.config, config);

                _debug('init called');

                this.eventBindings();

                // initialize pager instance
                window.pager = new Pagination({page: 1});

                
                this.fetch(pager.getPage());
                this.ui.$pagination.fadeIn();
            },
            eventBindings: function(){

                this.ui.$prevBtn.click(function(e){
                    e.preventDefault();
                    _debug('prev btn clicked');
                    pager.prevPage();
                    _debug(pager.getPage());
                    memberDirectory.fetch(pager.getPage(e));
                    memberDirectory.ui.setBtnState();
                });

                this.ui.$nextBtn.click(function(e){
                    e.preventDefault();
                    _debug('next btn clicked');
                    pager.nextPage();
                    _debug(pager.getPage());
                    memberDirectory.fetch(pager.getPage());
                    memberDirectory.ui.setBtnState();
                    
                });

                this.ui.$form.submit(function(e){
                    e.preventDefault();
                    var meta_val = $('#search_for').val(),
                        meta_key = $('#search_by').val();

                    _debug(meta_key);
                    _debug(meta_val);

                    memberDirectory.search({key: meta_key, val: meta_val});
                })


            },
            fetch: function(page){
                _debug('fetching: ' + page);

                memberDirectory.ui.loader.fadeIn();
                

                if (memberDirectory.cache[page]){

                    _debug('cache hit!');
                    memberDirectory.render(memberDirectory.cache[page]);

                } else {

                    $.ajax({
                        url: ajaxUrl,
                        data: {
                            'action': 'get_members',
                            'per_page': memberDirectory.config.per_page,
                            'offset': (pager.getPage() - 1) * memberDirectory.config.per_page // Pagers are 0-indexed on server side
                        },
                        success: function(data){
                            // Parse response to json
                            var json = JSON.parse(data);
                            // Pass the members data to be rendered
                            _debug(json);
                            pager.setPageLimit(json.num_pages);
                            memberDirectory.ui.setBtnState();
                            memberDirectory.render(json.data);
                        },
                        error: function(errorThrown){
                            log(errorThrown);
                        }
                    });

                    
                }

                
            },
            search: function(data){

                $.ajax({
                        url: ajaxUrl,
                        data: {
                            'action': 'search_members',
                            'per_page': memberDirectory.config.per_page,
                            'offset': (pager.getPage() - 1) * memberDirectory.config.per_page, // Pagers are 0-indexed on server side
                            'meta_key': data.key,
                            'meta_val': data.val
                        },
                        success: function(data){
                            // Parse response to json
                            var json = JSON.parse(data);
                            // Pass the members data to be rendered
                            _debug(json);
                            pager.setPageLimit(json.num_pages);
                            memberDirectory.ui.setBtnState();
                            memberDirectory.render(json.data);
                        },
                        error: function(errorThrown){
                            log(errorThrown);
                        }
                    });

            },
            render: function(members) {
                // Grab template
                var template = memberDirectory.ui.memberTmpl;
        
                //Clear any previously rendered items
                memberDirectory.ui.$outlet.html('');

                //cache data
                memberDirectory.cache[pager.getPage()] = members;

                //Loop through and render each member
                $.each(members, function(index, member){
                    // Keep admin/ doejo user out of Membership Directory results
                    if (member.ID === 1 || member.display_name === "Bryan Paronto") return;
                    // if (member)
                    memberDirectory.ui.$outlet.append(_.template(template, member));
                });

                // Fade out spinner
                memberDirectory.ui.loader.fadeOut();

                _debug(members);
                
            }

        };


        $(function(){
            if (!is_member_detail){
                memberDirectory.init({ debug: true});
            }
        });

    })(jQuery);