(function($){

   
    if (!window.is_album && window.albums) {
        renderAlbums(albums);
    }

    if (window.is_album && window.album) {
        renderAlbum(album);
    }

    
    function renderAlbums(albums){
        var $outlet = $('.albums-images .wrapper'),
            tmpl = $('#album-tmpl').html();

       _.each(albums, function (album){
            $outlet.append(_.template(tmpl, album));
       });

       $('.album-images').trigger('album:ready');
    }
    

    $('.album-images').on('album:ready', function(){
         $('.image').magnificPopup();
            log('albumready!');
    });




    function renderAlbum(album){
        var $outlet = $('.album-images .wrapper'),
            $header_outlet = $('.album-header .wrapper'),
            img_tmpl = $('#image-tmpl').html(),
            header_tmpl = $('#header-tmpl').html(),
            images;

        $header_outlet.append(_.template(header_tmpl, album));

        images = album.images;

        _.each(images, function (image){
            $outlet.append(_.template(img_tmpl, image));
        });

        // log(album);
    }

})(jQuery);





