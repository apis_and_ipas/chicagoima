window.log = function(){
  log.history = log.history || [];   // store logs to an array for reference
  log.history.push(arguments);
  if(this.console){
    console.log( Array.prototype.slice.call(arguments) );
  }
};


;(function($){

    function isMobile(){
        return ($(window).width() < '768');
    }

    $(function(){
        var mobileNav = new MobileNav().init();
        var eventHover = new EventsModule().init();

       $('.single-event .speaker, .page-about .board-member').infoToggler();
       $('.great-effort .content').equalHeights();
       $('.block-wrapper > div').equalHeights();

       $('.current_user').hover(function(){
          $(this).find('.sub-menu').toggleClass('active');
       });
       
        $('.newsletter-body').on('click', function(){
            $('.newsletter-hidden').slideToggle();
            $('.newsletter-body .sprite').toggleClass("rotate1 rotate2");
        });

        $('.item-toggle').on('click', function(){
            var $this = $(this);
            log('this thing loaded?');
            $this.siblings('.item-content').slideToggle();
            $this.children('span').toggleClass("rotate1 rotate2");
        });

        $('.single-event .event-map .module-header').hover(function(){
            $('.share-mouseover').hide();
            $('.event-share').show();
        }, function(){
            $('.share-mouseover').show();
           $('.event-share').hide();
        });

        $('.single-job_posting .job-detail .share-btns').hover(function(){
            $('.share-mouseover').hide();
            $('.job-share').show();
        }, function(){
            $('.share-mouseover').show();
           $('.job-share').hide();
        });

        // Send user back a page, retains search results in Job Board
       $('.single-job_posting #back-link').click(function(e){
          e.preventDefault(); 
          window.history.back();
        });
       
    });

    $(window).resize(function(){
        if (!isMobile()) {
            $('.page-get-involved .great-effort .content').equalHeights();
            $('.block-wrapper > div').equalHeights();
            // $('.blog .blog-post').equalHeights();
        }
        // $('.blog .blog-post').equalHeights();
    });

    

})(jQuery);

