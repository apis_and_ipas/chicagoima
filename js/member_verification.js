(function($){

    
    MemberVerify = (function(){

        var UI = {
            checkout_form: $('#checkout_form'),
            verify_agency_form: $('#verify_agency_form'),
            unverified_agency_form: $('#unverified_agency_form'),
            verify_student_form: $('#verify_student_form'),
            unverified_student_form: $('#unverified_student_form'),
            verification_forms: $('.verification_form')
        };

        function init(){
            setViewMode(window.signup_mode);
            UI.verify_student_form.submit(handleFormSubmission);
            UI.verify_agency_form.submit(handleFormSubmission);

            UI.verify_student_form.on('student:verified', studentVerified);
            UI.verify_agency_form.on('agency:verified', agencyVerified);

            UI.verify_student_form.on('student:failed', studentFailed);
            UI.verify_agency_form.on('agency:failed', agencyFailed);
        }

        function setViewMode(mode){
            switch (mode){
                case 'agency':
                    agency_verify();                    
                break;

                case 'student':
                    student_verify();
                break;

                case 'normal':
                 default:
                    no_verify();
                break;
            }
        }

        function agency_verify(){
            UI.checkout_form.hide();
            UI.verify_agency_form.show();

        }

        function student_verify(){
            UI.checkout_form.hide();
            UI.verify_student_form.show();
        }

        function no_verify(){
            UI.checkout_form.show();
            UI.verify_agency_form.hide();
            UI.verify_student_form.hide();
        }

        function handleFormSubmission(e){
            e.preventDefault();
            // log('verify form submitted');
            var $this = $(e.target);
            
            var form_action = $this.attr('id'),
                form_value = $this.find('[name=verify_email]').val(),
                validation_type = $this.data('type');

            $this.find('[type=submit]').attr('value','Verifiying...');

            $.ajax({
                url: ajaxUrl,
                data: {
                    'action': form_action,
                    'email': form_value,
                    'type' : validation_type
                },
                success: function(data){
                    
                    var json = JSON.parse(data);
                    log(json);

                    if (json.verified) {
                        $this.trigger(json.type + ':verified');
                    } else if (json.verified === false){

                        $this.trigger(json.type + ':failed');
                        $this.find('[type=submit]').attr('value','Oh no...');

                    }
                    setTimeout(function(){
                        $this.find('[type=submit]').attr('value','Verify');
                    }, 3000);
                },
                error: function(errorThrown){
                    log(errorThrown);
                }
            });
        }

        function studentVerified(){
            log('student verified');
            UI.verify_student_form.hide();
            UI.unverified_student_form.hide();
            UI.checkout_form.show();
        }

        function studentFailed(){
            log('student failed');
            UI.unverified_student_form.show();
        }

        function agencyVerified(){
            log('agency verified');
            UI.verify_agency_form.hide();
            UI.unverified_agency_form.hide();
            UI.checkout_form.show();
        }

        function agencyFailed(){
            log('student failed');
            UI.unverified_agency_form.show();
        }

        return { init: init }
    })();


    // DOM Ready
    $(function(){
        MemberVerify.init();
    });

})(jQuery);