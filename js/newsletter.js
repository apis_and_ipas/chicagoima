(function($){

    $(function(){
        var $form = $('#newsletter-signup'),
            $submit = $form.find('[type=submit]'),
            $input = $('[name="subscriber_email"]'),
            $errors = $('.parsley-error-list')
        ;

        function submitSuccess(response){
            var resp = JSON.parse( response.trim() );
            $errors && $errors.empty();

            log('Exact Target API response: ', resp);

            if (resp.status === "OK") {
                $form[0].reset()
                $submit.attr('value', 'Thanks!');
                
                // $thanks.reveal();
                setTimeout(function(){
                     $submit.removeAttr('disabled')
                            .attr('value', 'Submit');
                }, 8000);
            } else {
                
                $form[0].reset()

                $submit.removeAttr('disabled')
                            .attr('value', 'Submit');

                $input.addClass('parsley-validated parsley-error')
                    .after('<ul class="parsley-error-list"><li style="display: list-item;">' + resp.message + '</li></ul>')
                    .on('focus', function(){
                        $('.parsley-error-list').remove();
                        $input.removeClass('parsley-validated parsley-error')
                        
                    });
            
                
            }
        }

        function submitError(xhr, ajaxOptions, thrownError){
            log('Error! status: ' +  xhr.status);
            log('Error thrown: ' + thrownError);
        }

        $form.on("submit", function(e) {
          e.preventDefault();

          var $this = $(this),
              $subscriber_email = $this.find('#subscriber_email'),
              form_data;

          if ( $(this).parsley('validate') ) {
                $this.find('[type=submit]').attr('disabled','disabled').attr('value', 'Sending...');
                
                form_data = {
                  subscriber_email: $subscriber_email.val(),
                };

                console.log(form_data);

                $.ajax({
                    type: 'POST',
                    url: window.ajaxUrl,
                    data: {
                        "action": "create_subscriber",
                        "data": form_data
                    },
                    success: submitSuccess,
                    error: submitError
                });//ajax post
          }

        });
    });

})(jQuery);