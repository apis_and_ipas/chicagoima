
function MobileNav(options){
    this.defaults = {
        'trigger'     :  '.mobile-bars',
        'target'      :  '.mobile-nav',
        'activeClass' :  'active',
        'debug'       :   false
    };

    var opts = $.extend(true, {}, this.defaults, options);

    var $trigger = $(opts['trigger']),
        $target = $(opts['target']),
        activeClass = opts['activeClass'];

      return {
          init : function(){
              $trigger.on('click', function(){
                  $target.slideToggle().toggleClass(activeClass);
                  $trigger.toggleClass('rotate1 rotate2');
              });

              $(window).on('resize', function(){
                  var width =  $(this).width();

                  if (opts['debug']) console.log('Has active class? ' + !!$target.hasClass(activeClass));

                  if (width >= 1030 && $target.hasClass(activeClass)) {
                      $target.removeClass(activeClass).css('display','none');
                  }
              });
          }
      };
  }

    


      

     

