function EventsModule(options) {
    this.defaults = {
      'target' : '.event-listing',
      'debug'  : false
    };

    var opts = $.extend(true, {}, this.defaults, options);

    return {
        init: function(){
            if (opts['debug']) console.log('init');
            
            $(opts['target']).hover(function(){
                // var $this = $(this);
                $(opts['target']).not(this).stop().animate({opacity: 0.25}, 500);
            },function(){
                 $(opts['target']).stop().animate({opacity: 1}, 100);
            });
        }
    };
}