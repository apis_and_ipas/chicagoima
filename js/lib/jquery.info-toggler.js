;(function( $, window, document, undefined){

    $.fn.infoToggler = function(options){
        if (!this.length) { return this; }

        // var defaults = {};

        var opts = options; //$.extend(true, {}, defaults, options);

        function isMobile(){
            return ($(window).width() < '768');
        }

        this.each(function() {
            var $target = $(this);
            $target.click(function(){
                var $this = $(this),
                tmpl_id = $this.data('tmpl-id'),
                tmpl = $('#' + tmpl_id).html(),
                $outlet_container = $this.parent().next('.outlet'),
                outlet = $outlet_container.find('.wrapper');

                  if (!isMobile()){

                    if( $this.hasClass('active-item')){
                        $('.faded').removeClass('faded');
                        $this.removeClass('active-item');
                        $outlet_container.slideUp().removeClass('open');

                        // console.log('is current active-item. toggle closed');
                    } else if ($this.siblings('.active-item').length !== 0) {
                        $('.active-item').removeClass('active-item');
                        $this.addClass('active-item');
                        outlet.html(tmpl).hide().fadeIn();

                        // console.log('is active-item sibling. switch info, but keep outlet open');
                        // console.log($this.siblings('active-item').length);
                    } else {
                        $('.active-item').removeClass('active-item');
                        $this.addClass('active-item');

                        $('.outlet').slideUp();

                        $outlet_container.slideDown().addClass('open');
                        outlet.html(tmpl).hide().fadeIn();

                        //console.log('new speaker active');
                    }

                  } else if (isMobile()){ //Fires on mobile only to clean up.

                     $this.removeClass('active-item');
                     $outlet_container.slideUp().removeClass('open');
                  }
           
            });
        });

    };

    return this;

})(jQuery, window, document);