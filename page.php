<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/views/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for WPHelper can be found in the /functions sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = CIMA::get_context();
$post = new TimberPost();
$context['post'] = $post;

// Load extra page content
switch ($post->post_name) {

  case 'about':
    $context['board_members'] = CIMA::get_posts_by_type('board-member', false, 'ASC', 'menu_order');
    break;

  case 'get-involved':
    $context['great_efforts'] = CIMA::get_posts_by_type('great-effort', '10', 'ASC');
    break;

  case 'speak':
    require_once dirname(__FILE__). '/inc/includes/industries.php';
    $context['industries'] = $industries;
    break;

  case 'sponsors':
    $context['sponsors'] = CIMA::get_posts_by_type('sponsor', false, 'ASC');
    break;

  case 'become-a-member':
    // This route is deprecated due to redirect issues, 
    //  instead link here: '/account/membership-levels/'
    wp_redirect( '/account/membership-levels/' );
    break;

  case 'member-directory':
    wp_enqueue_script( 'underscore' );
    if (isset($_REQUEST['member'])){
      $context['is_member_detail'] = true;
      $id = sanitize_text_field(  $_REQUEST['member']  );
      $user_obj = get_userdata( $id );
      $user_with_meta = CIMA_Member_Directory::getUserMeta( $user_obj , true);
      $context['member_details'] = $user_with_meta;
      // var_dump( $user_with_meta );
    } else {
      $context['is_member_detail'] = false;
    }
    break;

  case 'gallery':
    global $cima_gallery;
    if (isset($_REQUEST['fb_album'])){
      $context['is_album'] = true;
      $album_id = $_REQUEST['fb_album'];
      $context['album'] = json_encode( $cima_gallery->get_album($album_id) );
    
    } else {
      $context['is_album'] = false;
      $context['albums'] = json_encode( $cima_gallery->get_albums() );
    }
   
    break;
    
  default:
    # code...
    break;
}

CIMA::render(array('page-' . $post->post_name . '.twig', 'page.twig'), $context);