<?php       
    global $gateway, $pmpro_review, $skip_account_fields, $pmpro_paypal_token, $wpdb, $current_user, $pmpro_msg, $pmpro_msgt, $pmpro_requirebilling, $pmpro_level, $pmpro_levels, $tospage, $pmpro_currency_symbol, $pmpro_show_discount_code, $pmpro_error_fields;
    global $discount_code, $username, $password, $password2, $bfirstname, $blastname, $baddress1, $baddress2, $bcity, $bstate, $bzipcode, $bcountry, $bphone, $bemail, $bconfirmemail, $CardType, $AccountNumber, $ExpirationMonth,$ExpirationYear; 
    
    $pmpro_stripe_lite = apply_filters("pmpro_stripe_lite", !pmpro_getOption("stripe_billingaddress")); //default is oposite of the stripe_billingaddress setting
?>
<?php 
$signup_mode = null;
 
switch ($pmpro_level->id) {
    case 3:
      $signup_mode = "normal";
      break;

    case 2:
     $signup_mode = "student";
     break;

    case 1:
     $signup_mode = "agency";
     break;
   
    default:
      # code...
      break;
 } 
 ?>
  
  <script>
    window.signup_mode = '<?php echo $signup_mode; ?>';
  </script>

<div class="splash">

    <div class="wrapper">
            <h1><?php printf("%s", $pmpro_level->name);?></h1>

            <p class="splash-text">
                <?php if ( $pmpro_level->initial_payment == $pmpro_level->billing_amount ):?>
                    <strong><?php printf("$%s Per %s", $pmpro_level->billing_amount, $pmpro_level->cycle_period);?></strong>
                <?php else:?>
                    <strong>
                        <?php printf("1st %s - %s", $pmpro_level->cycle_period, $pmpro_level->initial_payment ); ?></strong><br>
                        <?php printf("Renew for only %s Per %s", $pmpro_level->billing_amount, $pmpro_level->cycle_period ); ?></strong><br>
                    </strong>
                <?php endif;?>
             </p>

            <p class="splash-text">Thanks for your interest in becoming a member, fill out the form below to complete 
            your registration and welcome to the CIMA family!</p>

    </div><?php // wrapper ?>
    
</div><?php //splash ?>


<div class="body">
    <div class="wrapper ">
        <div class="main" >


          <div id="verify_agency_form" style="display:none;">
            <strong>Verify your Agency / Corporate affiliation!</strong>
            <p>Please provide your work/corporate provided email address below.</p>
            <form id="verify_agency" data-type="agency" class="verification_form" action="" parsley-validate>
              <label for="verify_email">Agency / Corporate email</label>
              <input type="text" name="verify_email" id="verify_email" parsley-type="email" parsley-required="true">
              <input type="submit" id="verify-submit" value="Verify">
            </form>
          </div>

          <div id="unverified_agency_form" style="display:none;">
            <?php echo get_field('unverified_agency', 'options') ?>
          </div>

          <div id="verify_student_form" style="display:none;">
            <strong>Verify your Student / Educator affiliation!</strong>
            <p>Please provide your school provided email address below.</p>
            <form id="verify_student" data-type="student" class="verification_form" action="" parsley-validate>
              <label for="verify_email">Student / Educator email</label>
              <input type="text" name="verify_email" id="verify_email" parsley-type="email" parsley-required="true">
              <input type="submit" id="verify-submit" value="Verify">
            </form>
          </div>

          <div id="unverified_student_form" style="display:none;">
            <?php echo get_field('unverified_student', 'options') ?>
          </div>
  
          <div id="checkout_form" style="display:none;">
            <?php require_once '_signup_form.php' ; ?>
          </div>
                    
        </div><?php //main ?>

        <div class="side">
          <div class="membership-includes">

            <header class="module-header">
              <span class="sprite pink-check"></span>
              Membership Includes
            </header>

            <div class="module-body">
             <?php echo $pmpro_level->description; ?> 
            </div>

          </div>
        </div><?php //side ?>

    </div><?php //wrapper ?>
</div><?php //body ?>


<script>
  (function($){ 

    /**
    *    Dirty Hack to fix Custom Post Type / Current menu-item issues
    */
    if ( $('body').hasClass('page-membership-checkout') ) {
       $('.current_page_parent').removeClass('current_page_parent');
       $('.menu-item-23').addClass('current-menu-item');
    }

  })(jQuery);
</script>
